//
//  ContentViewController.swift
//  RobertTest
//
//  Created by nus on 6/8/16.
//  Copyright © 2016 nus. All rights reserved.
//

import UIKit
import GoogleMaps

class ContentViewController: ParallaxTableViewViewController{
    
    let property:Dictionary<NSNumber,String> = [
        1:"Apartment",
        2:"Furnished Apartment",
        3:"Cotenancy Room (WG)",
        4:"Single Family House",
        5:"Parkplace Garage",
        6:"Holiday Object",
        7:"Multi Family Building",
        8:"Business Object",
        9:"Land",
        10:"Misc",
        11:"Loft",
        12:"Maisonette",
        13:"Studio",
        14:"Apartment_with_terrace",
        15:" Attic",
        16:"Penthouse",
        17:"Service Apartment",
        18:" Semidetached House",
        19:"Onefamily Townhouse",
        20:"Split Level House",
        21:"Villa",
        22:"Farm",
        23:"Parking Space",
        24:"Single Garage",
        25:"Underground Garage",
        26:"Tinker Room",
        27:"Building Land",
        28:"Business Land",
        29:"Holiday House",
        30:"Holiday Apartment",
        35:"One Family Row Hous",
    ]
    enum rowIndex:Int{
        case HEADER = 0
        case TITLE = 1
        case DETAILS = 2
        case LOCATION = 3
        case CONSTRUCTION = 4
        case NEARBY = 5
        case CONTACT = 6
    }
    let HEIGH_OF_TOP:CGFloat = 70
    let IDENTIFIER_HEADER_CELL = "HeaderCell"
    let IDENTIFIER_TITLE_CELL = "TitleCell"
    let IDENTIFIER_DETAILS_CELL = "DetailsCell"
    let IDENTIFIER_LOCATION_CELL = "LocationCell"
    let IDENTIFIER_CONTRUCTION_CELL = "ConstructionCell"
    let IDENTIFIER_NEARBY_CELL = "NearbyCell"
    let IDENTIFIER_CONTACT_CELL = "ContactCell"
    var latitude:Double = 0
    var longitude:Double = 0
    var currentAdvertisement:Advertisement?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mapViewContent.hidden = true
        self.streetViewContent.hidden = true
        self.tableView.registerNib(UINib(nibName: IDENTIFIER_HEADER_CELL, bundle: nil), forCellReuseIdentifier: IDENTIFIER_HEADER_CELL)
        self.tableView.registerNib(UINib(nibName: IDENTIFIER_TITLE_CELL, bundle: nil), forCellReuseIdentifier: IDENTIFIER_TITLE_CELL)
        self.tableView.registerNib(UINib(nibName: IDENTIFIER_DETAILS_CELL, bundle: nil), forCellReuseIdentifier: IDENTIFIER_DETAILS_CELL)
        self.tableView.registerNib(UINib(nibName: IDENTIFIER_LOCATION_CELL, bundle: nil), forCellReuseIdentifier: IDENTIFIER_LOCATION_CELL)
        self.tableView.registerNib(UINib(nibName: IDENTIFIER_CONTRUCTION_CELL, bundle: nil), forCellReuseIdentifier: IDENTIFIER_CONTRUCTION_CELL)
        self.tableView.registerNib(UINib(nibName: IDENTIFIER_NEARBY_CELL, bundle: nil), forCellReuseIdentifier: IDENTIFIER_NEARBY_CELL)
        self.tableView.registerNib(UINib(nibName: IDENTIFIER_CONTACT_CELL, bundle: nil), forCellReuseIdentifier: IDENTIFIER_CONTACT_CELL)
        getAdvertisementDetails()
    }
    
    // show status bar
    override func prefersStatusBarHidden() -> Bool {
        guard  let statusBar = UIApplication.sharedApplication().valueForKey("statusBarWindow")?.valueForKey("statusBar") as? UIView else {
            
            return false
        }
        statusBar.backgroundColor = UIColor.whiteColor()
        
        return false
    }
    
    //add map info
    func addMapInfo(){
         latitude = Double((currentAdvertisement?.geoCoordinates!["Latitude"])!)
         longitude = Double((currentAdvertisement?.geoCoordinates!["Longitude"])!)
    }
    
    // show normal map
    func showNormalMap(){
        self.mapViewContent.removeFromSuperview()
        let camera = GMSCameraPosition.cameraWithLatitude(latitude, longitude: longitude, zoom: 14)
        let  mapView = GMSMapView.mapWithFrame(CGRectZero, camera: camera)
        mapView.myLocationEnabled = true
        self.mapViewContent = mapView
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(latitude,longitude)
        marker.title = currentAdvertisement?.title
        marker.snippet = currentAdvertisement?.streetName
        marker.map = (self.mapViewContent as! GMSMapView)
        view.addSubview(mapView)
        view.sendSubviewToBack(mapView)
    }
    
    // show street view
    func showStreetView(){
        self.streetViewContent.removeFromSuperview()
        let streetView = GMSPanoramaView(frame: CGRectZero)
        streetView.moveNearCoordinate(CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
        streetView.camera = GMSPanoramaCamera(heading: 180, pitch: -10, zoom: 1)
        self.streetViewContent = streetView
        view.addSubview(streetView)
        view.sendSubviewToBack(streetView)
    }
    
    // load images
    func loadImages(){
        if currentAdvertisement?.imageUrls != nil{
            let count:Int =  (currentAdvertisement?.imageUrls!.count)!
            for(var i:Int = 0; i < count; i++ ){
                let imgView = UIImageView()
                let strUrl:String? = currentAdvertisement?.imageUrls![i]
                if let sEncode = strUrl!.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet()) {
                    let url =  NSURL(string: sEncode)
                    imgView.sd_setImageWithURL(url)
                }
                imgView.frame = CGRect(x: screenSize.size.width * CGFloat(i), y: 0, width: screenSize.size.width, height: imageHeight)
                imgView.contentMode = UIViewContentMode.ScaleAspectFill
                imgView.tag = i
                imgView.userInteractionEnabled = true
                let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:"onSelectImage:")
                 imgView.addGestureRecognizer(tap)
                scrollView.addSubview(imgView)
            }
            scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * CGFloat(count),scrollView.frame.size.height);
        }
    }
    
    //
    func onSelectImage(imgView: UITapGestureRecognizer){        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: InfoViewController = storyboard.instantiateViewControllerWithIdentifier("InfoViewController") as! InfoViewController
        let index = imgView.view!.tag
        vc.url =  currentAdvertisement?.imageUrls![index]
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    // MARK: - call API
    func getAdvertisementDetails(){
        MBProgressHUD.showHUDAddedTo(self.scrollView, animated: false)
        APIClient.sharedInstance.getAdvertisementDetails("", success: { (success, advertisement) -> () in
            self.currentAdvertisement = advertisement
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                MBProgressHUD.hideHUDForView(self.scrollView, animated: false)
                self.loadImages()
                self.tableView.reloadData()
                self.addMapInfo()
            })
            }) { (error) -> () in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    MBProgressHUD.hideHUDForView(self.scrollView, animated: false)
            })
        }
    }
    
    // MARK: - tableView Data Source
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if currentAdvertisement != nil{
            
            return 7
        }
        
        return 0
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch(indexPath.row){
        case rowIndex.HEADER.rawValue:
            
            return 70
        case rowIndex.TITLE.rawValue:
            let titleCell = tableView.dequeueReusableCellWithIdentifier(IDENTIFIER_TITLE_CELL) as! TitleCell
            titleCell.lbTitle.text = currentAdvertisement?.title
            let titleHeight:CGFloat = titleCell.lbTitle.heightForView(titleCell.lbTitle.text!, font: titleCell.lbTitle.font, width: titleCell.lbTitle.bounds.size.width - 15)
            titleCell.lbDescription.text = currentAdvertisement?.descriptionAd
            let descriptionHeight:CGFloat = titleCell.lbDescription.heightForView(titleCell.lbDescription.text!, font: titleCell.lbDescription.font, width: titleCell.lbDescription.bounds.size.width - 15)
            
            return titleHeight + descriptionHeight - 20
        case rowIndex.DETAILS.rawValue:
            
            return 300
        case rowIndex.LOCATION.rawValue:
            
            return 130
        case rowIndex.CONSTRUCTION.rawValue:
            let constructionCell = tableView.dequeueReusableCellWithIdentifier(IDENTIFIER_CONTRUCTION_CELL) as! ConstructionCell
            if var features = currentAdvertisement?.features{
                features = features.stringByReplacingOccurrencesOfString(",", withString: "\n\n")
                constructionCell.lbFeatures.text = " " + features
            }
            let height:CGFloat = constructionCell.lbFeatures.heightForView(constructionCell.lbFeatures.text!, font: constructionCell.lbFeatures.font, width: constructionCell.lbFeatures.bounds.size.width - 15)
            
            return height + HEIGH_OF_TOP
        case rowIndex.NEARBY.rawValue:
            let nearbyCell = tableView.dequeueReusableCellWithIdentifier(IDENTIFIER_NEARBY_CELL) as! NearbyCell
            if var nearby = currentAdvertisement?.distances{
                nearby = nearby.stringByReplacingOccurrencesOfString(",", withString: "\n\n")
                nearbyCell.lbNearby.text = " " + nearby
            }
            let height:CGFloat = nearbyCell.lbNearby.heightForView(nearbyCell.lbNearby.text!, font: nearbyCell.lbNearby.font, width: nearbyCell.lbNearby.bounds.size.width - 15)
            
            return height + HEIGH_OF_TOP
        default:
            //contact cell
            return 180
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch(indexPath.row){
        case rowIndex.HEADER.rawValue:
            let headerCell = tableView.dequeueReusableCellWithIdentifier(IDENTIFIER_HEADER_CELL, forIndexPath: indexPath) as! HeaderCell
            headerCell.lbTitle.text = currentAdvertisement?.streetName
            headerCell.lblAddress.text = (currentAdvertisement?.zip)! + " " + (currentAdvertisement?.city)!
            headerCell.lbPrice.text = (currentAdvertisement?.currencyID)! + " " + "\(currentAdvertisement!.priceValue!)"
            headerCell.delegate = self
            
            return headerCell
        case rowIndex.TITLE.rawValue:
            let titleCell = tableView.dequeueReusableCellWithIdentifier(IDENTIFIER_TITLE_CELL, forIndexPath: indexPath) as! TitleCell
            titleCell.lbTitle.text = currentAdvertisement?.title
            titleCell.lbDescription.text = currentAdvertisement?.descriptionAd
            
            return titleCell
        case rowIndex.DETAILS.rawValue:
            let detailsCell = tableView.dequeueReusableCellWithIdentifier(IDENTIFIER_DETAILS_CELL, forIndexPath: indexPath) as! DetailsCell
            if let propertyValue = property[currentAdvertisement!.propertyTypeID!]{
                detailsCell.lbPropertyType.text = "\(propertyValue)"
            }else{
                detailsCell.lbPropertyType.text = "N/A"
            }
            var supplementaryCharge:Int = 0
            if let extraCost = currentAdvertisement?.extraCosts  {
                
                supplementaryCharge = Int((currentAdvertisement!.priceValue)!) - Int(extraCost)
            }
            detailsCell.lbRentPerMonth.text = "\(currentAdvertisement!.currencyID!) \(currentAdvertisement!.priceValue!)"
            detailsCell.lbRentWithoutCharge.text = "\(currentAdvertisement!.currencyID!) \(supplementaryCharge)"
            detailsCell.lbSupplementary.text = "\(currentAdvertisement!.currencyID!) \(currentAdvertisement!.extraCosts!)"
            if let userfulArea = currentAdvertisement?.usefulArea{
                detailsCell.lbFloor.text = "\(userfulArea) m²"
            }
            detailsCell.lbRoom.text = "\(currentAdvertisement!.rooms!)"
            detailsCell.lbYear.text = "\(currentAdvertisement!.yearOfConstruction!)"
            
            return detailsCell
        case rowIndex.LOCATION.rawValue:
            let locationCell = tableView.dequeueReusableCellWithIdentifier(IDENTIFIER_LOCATION_CELL, forIndexPath: indexPath) as! LocationCell
            if let locationfactor = currentAdvertisement?.locationFactors{
                let altitude:NSNumber = locationfactor["Height"] as! NSNumber
                let summerHour:NSNumber = locationfactor["SunshineHoursSummer"] as! NSNumber
                let winterHour:NSNumber = locationfactor["SunshineHoursWinter"] as! NSNumber
                locationCell.lbAltitude.text = "\(altitude) m. above sea level"
                locationCell.lbSunshine.text = "\(summerHour)h (summer)\n\(winterHour)h (winter)"
            }
            
            return locationCell
        case rowIndex.CONSTRUCTION.rawValue:
            let constructionCell = tableView.dequeueReusableCellWithIdentifier(IDENTIFIER_CONTRUCTION_CELL, forIndexPath: indexPath) as! ConstructionCell
            if var features = currentAdvertisement?.features{
                features = features.stringByReplacingOccurrencesOfString(",", withString: "\n\n")
                constructionCell.lbFeatures.text = " " + features
            }
            
            return constructionCell
            
        case rowIndex.NEARBY.rawValue:
            let nearbyCell = tableView.dequeueReusableCellWithIdentifier(IDENTIFIER_NEARBY_CELL, forIndexPath: indexPath) as! NearbyCell
            if var nearby = currentAdvertisement?.distances{
                nearby = nearby.stringByReplacingOccurrencesOfString(",", withString: "\n\n")
                nearbyCell.lbNearby.text = " " + nearby
            }
            
            return nearbyCell
        default:
            //contact
            let contactCell = tableView.dequeueReusableCellWithIdentifier(IDENTIFIER_CONTACT_CELL, forIndexPath: indexPath) as! ContactCell
            contactCell.lbContactName.text = currentAdvertisement?.contact?.contactName!
            contactCell.lbVendorContact.text = currentAdvertisement?.contact?.vendorContact!
            contactCell.tvContactView.text = currentAdvertisement?.contact?.contactTel
            contactCell.tvContactView.editable = false
            contactCell.tvContactView.dataDetectorTypes = UIDataDetectorTypes.All
            
            return contactCell
        }
    }
}

extension ContentViewController:HeaderCelDelegate{
    func showNormalMapOption(){
        self.streetViewContent.hidden = true
        let hidden = self.mapViewContent.hidden
        if hidden {
            self.mapViewContent.hidden = false
            scrollView.hidden = true
            self.showNormalMap()
        }else{
            self.mapViewContent.hidden = true
            scrollView.hidden = false
        }
    }
    
    func showStreetViewOption(){
        self.mapViewContent.hidden = true
        let hidden = self.streetViewContent.hidden
        if hidden {
            self.streetViewContent.hidden = false
            scrollView.hidden = true
            self.showStreetView()
        }else{
            self.streetViewContent.hidden = true
            scrollView.hidden = false
        }
    }
}
