//
//  ContactCell.swift
//  RobertTest
//
//  Created by nus on 6/14/16.
//  Copyright © 2016 nus. All rights reserved.
//

import UIKit

class ContactCell: UITableViewCell {

    @IBOutlet weak var lbVendorContact: UILabel!
    @IBOutlet weak var lbContactName: UILabel!
    
    @IBOutlet weak var tvContactView: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
