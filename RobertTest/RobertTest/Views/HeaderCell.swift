//
//  HeaderCell.swift
//  RobertTest
//
//  Created by nus on 6/13/16.
//  Copyright © 2016 nus. All rights reserved.
//

import UIKit

@objc protocol HeaderCelDelegate:class{
    optional
    func showNormalMapOption()
    optional
    func showStreetViewOption()
}


class HeaderCell: UITableViewCell {

    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var btnMap: UIButton!
    @IBOutlet weak var btnStreet: UIButton!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lbPrice: UILabel!
    
    var isHiddenMap:Bool = true
    var isHiddenStreet:Bool = true
    
    weak var delegate:HeaderCelDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onShowMap(sender: AnyObject) {
         btnStreet.setImage(UIImage(named: "street"), forState: UIControlState.Normal)
         isHiddenStreet = true
        if isHiddenMap{
            btnMap.setImage(UIImage(named: "gallery"), forState: UIControlState.Normal)
            isHiddenMap = false
        }else{
            btnMap.setImage(UIImage(named: "map"), forState: UIControlState.Normal)
            isHiddenMap = true
        }
        if self.delegate != nil{
            self.delegate?.showNormalMapOption?()
        }
    }
    
    @IBAction func onShowStreetView(sender: AnyObject) {
        btnMap.setImage(UIImage(named: "map"), forState: UIControlState.Normal)
         isHiddenMap = true
        if isHiddenStreet {
            btnStreet.setImage(UIImage(named: "gallery"), forState: UIControlState.Normal)
            isHiddenStreet = false
        }else{
            btnStreet.setImage(UIImage(named: "street"), forState: UIControlState.Normal)
            isHiddenStreet = true
        }
        if self.delegate != nil{
            self.delegate?.showStreetViewOption?()
        }
    }
}
