//
//  ParallaxTableViewViewController.swift
//  RobertTest
//
//  Created by nus on 6/8/16.
//  Copyright © 2016 nus. All rights reserved.
//

import UIKit
import GoogleMaps

class ParallaxTableViewViewController: UITableViewController {
    
    let scrollView = UIScrollView()
    var currentPage:Int = 0
    let screenSize:CGRect = UIScreen.mainScreen().bounds
    var mapViewContent = UIView()
    var streetViewContent = UIView()
    // Set the factor for the parallaxEffect. This is overwritable.
    var parallaxFactor:CGFloat = 2
    
    // Set the default height for the image on the top.
    var imageHeight:CGFloat = (UIScreen.mainScreen().bounds.height / 2 - 60) {
        didSet {
            moveScrollView()
        }
    }
    
    // Initialize the scrollOffset varaible.
    var scrollOffset:CGFloat = 0 {
        didSet {
            moveScrollView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set the contentInset to make room for the image.
        self.tableView.contentInset = UIEdgeInsets(top: imageHeight, left: 0, bottom: 0, right: 0)
        scrollView.pagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.delegate = self
        view.addSubview(scrollView)
        view.sendSubviewToBack(scrollView)
        tableView.allowsSelection = false
    }
    
    override func viewDidLayoutSubviews() {
        self.tableView.frame = CGRect(x: 0, y:20, width: self.tableView.frame.width, height: self.tableView.frame.height)
        // Update the image position after layout changes.
        moveScrollView()
    }
    
    // Define method for image location changes.
    func moveScrollView() {
        let imageOffset = (scrollOffset >= 0) ? scrollOffset / parallaxFactor : 0
        let imageHeight = (scrollOffset >= 0) ? self.imageHeight : self.imageHeight - scrollOffset
        let frame = CGRect(x: 0, y: -imageHeight + imageOffset, width: view.bounds.width, height: imageHeight)
        scrollView.frame = frame
        mapViewContent.frame = CGRect(x: 0, y: -imageHeight + imageOffset, width: view.bounds.width, height: imageHeight)
        streetViewContent.frame = CGRect(x: 0, y: -imageHeight + imageOffset, width: view.bounds.width, height: imageHeight)
        if(scrollView.subviews.count > 0){
            let page:CGFloat = scrollView.contentOffset.x / scrollView.frame.size.width
            let index:Int = Int(page)
            let subview = scrollView.subviews[index]
            if subview.isKindOfClass(UIImageView){
                subview.frame = CGRect(x: screenSize.size.width * page, y: 0, width: screenSize.size.width, height: imageHeight)
            }
            // first setup
            if currentPage == 0 && scrollView.subviews.count > 1{
                let nextImgView = scrollView.subviews[1]
                if nextImgView .isKindOfClass(UIImageView){
                    (nextImgView as! UIImageView).contentMode = UIViewContentMode.ScaleToFill
                }
            }
        }
    }
    
    // MARK: - UIScrollView delegate
    // Update scrollOffset on tableview scroll
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView.isKindOfClass(UITableView){
            scrollOffset = tableView.contentOffset.y + imageHeight
            
            return
        }
        if scrollView.isKindOfClass(UIScrollView){
            let page:CGFloat = scrollView.contentOffset.x / scrollView.frame.size.width
            let index:Int = Int(page)
            if index != currentPage{
                if index < scrollView.subviews.count - 1{
                    let nextImgView = scrollView.subviews[index + 1]
                    if nextImgView .isKindOfClass(UIImageView){
                        (nextImgView as! UIImageView).contentMode = UIViewContentMode.ScaleToFill
                    }
                }
                if index > 0 {
                    let previousImgView = scrollView.subviews[index - 1]
                    if previousImgView .isKindOfClass(UIImageView){
                        (previousImgView as! UIImageView).contentMode = UIViewContentMode.ScaleToFill
                    }
                }
                currentPage = index
            }
        }
    }
    
   override func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        let page:CGFloat = scrollView.contentOffset.x / scrollView.frame.size.width
        let index:Int = Int(page)
        if scrollView .isKindOfClass(UITableView){
        
            return
        }
        if currentPage == 0 || currentPage == (scrollView.subviews.count - 1) {
            let nextImgView = scrollView.subviews[1]
            if nextImgView.isKindOfClass(UIImageView){
                (nextImgView as! UIImageView).contentMode = UIViewContentMode.ScaleAspectFill
            }
        
            return
        }
        if index == currentPage{
            if index < scrollView.subviews.count - 1{
                let nextImgView = scrollView.subviews[index + 1]
                if nextImgView.isKindOfClass(UIImageView){
                    (nextImgView as! UIImageView).contentMode = UIViewContentMode.ScaleAspectFill
                }
            }
            if index > 0 {
                let previousImgView = scrollView.subviews[index - 1]
                if previousImgView.isKindOfClass(UIImageView){
                    (previousImgView as! UIImageView).contentMode = UIViewContentMode.ScaleAspectFill
                }
            }
            currentPage = index
        }
    }
}
