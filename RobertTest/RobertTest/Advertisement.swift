//
//  Advertisement.swift
//  RobertTest
//
//  Created by nus on 6/10/16.
//  Copyright © 2016 nus. All rights reserved.
//

import UIKit

class Advertisement{

    var appConfig:String?
    var adID:NSNumber?
    var advertisementEntries:String?
    var area:NSNumber?
    var availability:String?
    var buildingArea:String?
    var changeDate:String?
    var city:String?
    var comparisPoints:NSNumber?
    var comparisPrice:NSNumber?
    var comparisUrl:String?
    var contact:Contact?
    var contactSiteID:NSNumber?
    var contactSiteName:String?
    var currencyID:String?
    var dealTypeID:NSNumber?
    var descriptionAd:String?
    var detailImpressionTrackingUrl:String?
    var distances:String?
    var extraCosts:NSNumber?
    var features:String?
    var floor:String?
    var geoCoordinates:Dictionary<String,NSNumber>?
    var imageCount:NSNumber?
    var imageUrls:Array<String>?
    var isActive:Bool?
    var isAddressDistinct:Bool?
    var isStuffleAd:Bool?
    var lastRelevantChangeDate:String?
    var livingSpace:String?
    var locationFactors:Dictionary<String,AnyObject>?
    var overallPriceChange:NSNumber?
    var phoneCallTrackingUrl:String?
    var price:NSNumber?
    var priceDevelopment:NSNumber?
    var priceType:NSNumber?
    var priceValue:NSNumber?
    var propertyTypeID:NSNumber?
    var releaseDate:String?
    var rentInclusiveHeating:NSNumber?
    var rooms:NSNumber?
    var savingPotential:String?
    var siteID:NSNumber?
    var siteName:String?
    var siteSource:NSNumber?
    var streetName:String?
    var subPropertyTypeID:NSNumber?
    var title:String?
    var usefulArea:NSNumber?
    var yearOfConstruction:NSNumber?
    var zip:String?
}

extension Advertisement{
    
    class func JSONKeyPathsByPropertyKeyManual (dic: Dictionary<String, AnyObject>) -> Advertisement {
        let data:Advertisement = Advertisement()
        data.appConfig = dic["AdConfig"] as? String
        data.adID = dic["AdId"] as? NSNumber
        data.advertisementEntries = dic["AdvertisementEntries"] as? String
        data.area = dic["Area"] as? NSNumber
        data.availability = dic["Availability"] as? String
        data.buildingArea = dic["BuildingArea"] as? String
        data.changeDate = dic["ChangeDate"] as? String
        data.city = dic["City"] as? String
        data.comparisPoints = dic["ComparisPoints"] as? NSNumber
        data.comparisPrice = dic["ComparisPrice"] as? NSNumber
        data.comparisUrl = dic["ComparisUrl"] as? String
        var contactData:Dictionary<String,AnyObject>? = dic["Contact"] as? Dictionary<String,AnyObject>
        data.contact = Contact()
        let vendor = contactData!["VendorContact"] as? String
        data.contact?.vendorContact = vendor
        let name = contactData!["ContactName"] as? String
        data.contact?.contactName = name
        let tel = contactData!["ContactTel"] as? String
        data.contact?.contactTel = tel
        let comment = contactData!["ContactComment"] as? String
        data.contact?.contactComment = comment
        data.contactSiteID = dic["ContactSiteID"] as? NSNumber
        data.contactSiteName = dic["ContactSiteName"] as? String
        data.currencyID = dic["CurrencyID"] as? String
        data.dealTypeID = dic["DealTypeID"] as? NSNumber
        data.descriptionAd = dic["Description"] as? String
        data.detailImpressionTrackingUrl = dic["DetailImpressionTrackingUrl"] as? String
        data.distances = dic["Distances"] as? String
        data.extraCosts = dic["ExtraCosts"] as? NSNumber
        data.features = dic["Features"] as? String
        data.floor = dic["Floor"] as? String
        data.geoCoordinates = dic["GeoCoordinates"] as? Dictionary<String,NSNumber>
        data.imageCount = dic["ImageCount"] as? NSNumber
        data.imageUrls = dic["ImageUrls"] as? Array<String>
        data.isActive = dic["IsActive"] as? Bool
        data.isAddressDistinct = dic["IsAddressDistinct"] as? Bool
        data.isStuffleAd = dic["IsStuffleAd"] as? Bool
        data.lastRelevantChangeDate = dic["LastRelevantChangeDate"] as? String
        data.livingSpace = dic["LivingSpace"] as? String
        data.locationFactors = dic["LocationFactors"] as? Dictionary<String,AnyObject>
        data.overallPriceChange = dic["OverallPriceChange"] as? NSNumber
        data.phoneCallTrackingUrl = dic["PhoneCallTrackingUrl"] as? String
        data.price = dic["Price"] as? NSNumber
        data.priceDevelopment = dic["PriceDevelopment"] as? NSNumber
        data.priceType = dic["PriceType"] as? NSNumber
        data.priceValue = dic["PriceValue"] as? NSNumber
        data.propertyTypeID = dic["PropertyTypeID"] as? NSNumber
        data.releaseDate = dic["ReleaseDate"] as? String
        data.rentInclusiveHeating = dic["RentInclusiveHeating"] as? NSNumber
        data.rooms = dic["Rooms"] as? NSNumber
        data.savingPotential = dic["SavingPotential"] as? String
        data.siteID = dic["SiteID"] as? NSNumber
        data.siteName = dic["SiteName"] as? String
        data.siteSource = dic["SiteSource"] as? NSNumber
        data.streetName = dic["StreetName"] as? String
        data.subPropertyTypeID = dic["SubPropertyTypeID"] as? NSNumber
        data.title = dic["Title"] as? String
        data.usefulArea = dic["UsefulArea"] as? NSNumber
        data.yearOfConstruction = dic["YearOfConstruction"] as? NSNumber
        data.zip = dic["Zip"] as? String
        
        return data;
    }
}
