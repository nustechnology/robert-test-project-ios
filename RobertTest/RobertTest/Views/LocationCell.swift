//
//  LocationCell.swift
//  RobertTest
//
//  Created by nus on 6/14/16.
//  Copyright © 2016 nus. All rights reserved.
//

import UIKit

class LocationCell: UITableViewCell {

    @IBOutlet weak var lbAltitude: UILabel!
    @IBOutlet weak var lbSunshine: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
