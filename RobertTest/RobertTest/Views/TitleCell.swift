//
//  TitleCell.swift
//  RobertTest
//
//  Created by nus on 6/14/16.
//  Copyright © 2016 nus. All rights reserved.
//

import UIKit

class TitleCell: UITableViewCell {

    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbDescription: UILabel!
    @IBOutlet weak var constraintDescription: NSLayoutConstraint!
    @IBOutlet weak var constraintTittle: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
