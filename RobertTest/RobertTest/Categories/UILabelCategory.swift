//
//  UILabelCategory.swift
//  RobertTest
//
//  Created by nus on 6/14/16.
//  Copyright © 2016 nus. All rights reserved.
//

import UIKit

extension UILabel {
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRectMake(0, 0, width, CGFloat.max))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
}
