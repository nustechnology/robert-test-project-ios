//
//  APIClient.swift
//  RobertTest
//
//  Created by nus on 6/10/16.
//  Copyright © 2016 nus. All rights reserved.
//

import UIKit

class APIClient{
    // MARK: Shared Instance
    class var sharedInstance : APIClient {
        struct Static {
            static var onceToken : dispatch_once_t = 0
            static var instance : APIClient? = nil
        }
        
        dispatch_once(&Static.onceToken) {
            
            Static.instance = APIClient()
        }
        
        return Static.instance!
    }
    
    func getAdvertisementDetails(advertisementId: String,success:(success: Bool, advertisement: Advertisement)->(),failure:(error: NSError?) ->()){
        
        let request = NSMutableURLRequest(URL: NSURL(string: "https://en.comparis.ch/immobilien/webservices/mobile_v2.svc/json/getdetails?requestObject=%7B%22AdId%22%3A14823955%2C%20%22Header%22%3A%7B%22ApplicationVersion%22%3Anull%2C%22BundleName%22%3Anull%2C%22Device%22%3Anull%2C%22DeviceApplicationGUID%22%3A%2200000000-0000-0000-0000-000000000000%22%2C%22Language%22%3A%22en%22%2C%22Model%22%3Anull%2C%22RequestKey%22%3A0%2C%22ScreenHeight%22%3A0%2C%22ScreenWidth%22%3A0%7D%7D")!,
            cachePolicy: .UseProtocolCachePolicy,
            timeoutInterval: 10.0)
        request.HTTPMethod = "GET"
        
        let session = NSURLSession.sharedSession()
        let dataTask = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                failure(error: error)
            } else {
                do {
                    let dic:Dictionary<String,AnyObject> = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as! [String:AnyObject]
                    let advertisement = Advertisement.JSONKeyPathsByPropertyKeyManual(dic)
                    success(success: true, advertisement: advertisement)
                } catch let error as NSError {
                    failure(error: error)
                }
            }
        })
        
        dataTask.resume()
    }
}
