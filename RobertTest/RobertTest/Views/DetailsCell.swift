//
//  DetailsCell.swift
//  RobertTest
//
//  Created by nus on 6/14/16.
//  Copyright © 2016 nus. All rights reserved.
//

import UIKit

class DetailsCell: UITableViewCell {

    @IBOutlet weak var lbPropertyType: UILabel!
    @IBOutlet weak var lbRentPerMonth: UILabel!
    @IBOutlet weak var lbRentWithoutCharge: UILabel!
    @IBOutlet weak var lbSupplementary: UILabel!
    @IBOutlet weak var lbFloor: UILabel!
    @IBOutlet weak var lbRoom: UILabel!
    @IBOutlet weak var lbYear: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
